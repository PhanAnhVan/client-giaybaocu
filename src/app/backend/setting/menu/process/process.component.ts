import { Component, OnInit, OnDestroy } from '@angular/core'
import { Validators, FormBuilder, FormGroup } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../../globals'

@Component({
    selector: 'app-menu-process',
    templateUrl: './process.component.html'
})
export class MenuProcessComponent implements OnInit, OnDestroy {
    public connect: any

    public token: any = {
        process: 'set/pagesgroup/process',

        pathGetrow: 'get/pagesgroup/getrow',

        pathGetPages: 'get/pages/getlist'
    }
    public id: number

    fm: FormGroup

    public listPages: any = []

    public listPageDetail: any = []

    public listPagesSelect: any = []

    constructor(
        public fb: FormBuilder,

        public router: Router,

        public toastr: ToastrService,

        public globals: Globals,

        private routerAct: ActivatedRoute
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id']
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case 'getPages':
                    if (res['status'] == 1) {
                        if (this.id && this.id != 0) {
                            let container = this.fm.value.container
                            container = typeof container === 'object' ? container : container != '' ? JSON.parse(container) : ''
                            this.listPagesSelect = container
                            container =
                                container && container.length > 0
                                    ? container.reduce((n, o, i) => {
                                          n[o] = o
                                          return n
                                      }, {})
                                    : {}
                            this.listPages = res.data.filter(item => {
                                item.check = container[item.id] ? true : false
                                return true
                            })
                        } else {
                            this.listPages = res['data']
                        }
                        let data = this.listPages.reduce((n, o, i) => {
                            if (!n[o.type]) {
                                n[o.type] = { data: [], type: '' }
                            }
                            return n
                        }, [])
                        for (let i = 0; i < this.listPages.length; i++) {
                            const element = this.listPages[i]
                            data[element.type].data.push(element)
                            data[element.type].type = element.type
                        }
                        this.listPageDetail = Object.values(data)
                    }

                    break

                case 'pagesGroupGetRow':
                    if (res['data']['id'] && +res['data']['id'] == this.id) {
                        let data = res['data']

                        this.fmConfigs(data)

                        this.getListPages()
                    }
                    break
                case 'pagesGroupProcess':
                    let type = res['status'] == 1 ? 'success' : res['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1000 })
                    if (res['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/setting/menu/get-list'])
                        }, 2000)
                    }
                    break
                default:
                    break
            }
        })
    }
    ngOnInit() {
        if (this.id && this.id != 0) {
            this.getRow()
        } else {
            this.fmConfigs()
        }
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getListPages() {
        this.globals.send({ path: this.token.pathGetPages, token: 'getPages' })
    }
    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1, orders: 0 }
        this.fm = this.fb.group({
            container: item.container ? item.container : '',

            name: [item.name ? item.name : '', [Validators.required]],

            note: item.note ? item.note : '',

            status: item.status && item.status == 1 ? true : false
        })
    }
    getRow() {
        this.globals.send({ path: this.token.pathGetrow, token: 'pagesGroupGetRow', params: { id: this.id } })
    }
    selectPages(e) {
        if (e.target.checked === true) {
            this.listPagesSelect.push(e.target.value)
        } else {
            this.listPagesSelect.splice(this.listPagesSelect.indexOf(e.target.value), 1)
        }
    }
    onSubmit() {
        var data: any = this.fm.value

        data.container = this.listPagesSelect

        data.status = data.status == true ? 1 : 0

        this.globals.send({ path: this.token.process, token: 'pagesGroupProcess', data: data, params: { id: this.id || 0 } })
    }
}
