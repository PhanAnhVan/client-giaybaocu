import { Component, OnInit, OnDestroy } from '@angular/core'
import { TableService } from '../../../services/integrated/table.service'
import { Router } from '@angular/router'
import { Globals } from '../../../globals'
import { AlertComponent } from '../../modules/alert/alert.component'
import { BsModalService } from 'ngx-bootstrap/modal'
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service'
import { ToastrService } from 'ngx-toastr'

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
    styleUrls: ['./getlist.component.css']
})
export class GetlistComponent implements OnInit, OnDestroy {
    public detail: any = []
    public cart: any = {}
    public id: number

    public connect
    modalRef: BsModalRef
    public token: any = {
        getlist: 'get/cart/getlist',
        getrow: 'get/cart/getrow',
        changeStatus: 'set/cart/changeStatus',
        getUnRead: 'get/dashboard/getUnRead'
    }
    public itemselect: any = {}
    public infoCustomer: any = { name: '', address: '', price_total: '', phone: '' }

    public cols = [
        { title: 'lblStt', field: 'index', show: false },
        { title: 'cart.code', field: 'code', show: true, filter: true },
        { title: 'cart.fullName', field: 'name_customer', show: true, filter: true },
        { title: 'cart.deliveryAddress', field: 'delivery_address', show: true, filter: true },
        { title: 'cart.day_start', field: 'day_start', show: true, filter: true },
        { title: 'cart.amountTotal', field: 'amount_total', show: true, filter: true },
        { title: 'cart.priceTotal', field: 'price_total', show: true, filter: true },
        { title: 'cart.delivery_status', field: 'delivery_status', show: true, filter: true }
    ]
    public colsselect = [
        { title: 'lblStt', field: 'index' },
        { title: 'cart.productName', field: 'name' },
        { title: 'lblImages', field: 'images' },
        { title: 'products.attribute', field: 'attribute' },
        { title: 'cart.amountTotal', field: 'amount' },
        { title: 'products.price', field: 'price' }
    ]
    public step = -1
    public cwstable = new TableService()
    public cartItem = new TableService()
    constructor(public globals: Globals, public router: Router, public modalService: BsModalService, public toastr: ToastrService) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getlist':
                    this.cwstable.sorting = { field: 'day_start', sort: 'DESC', type: '' }
                    this.cwstable._concat(res.data, true)
                    break

                case 'getrow':
                    res.data.detail.reduce((n, o, i) => {
                        if (o.attribute && o.attribute != '') {
                            let temp = JSON.parse(o.attribute)
                            o.attribute = temp
                        }
                        return n
                    }, [])
                    this.cartItem._concat(res.data.detail, true)
                    let cart = res.data.cart
                    this.infoCustomer.name = cart.name
                    this.infoCustomer.address = cart.delivery_address
                    this.infoCustomer.phone = cart.phone
                    break

                case 'changeStatus':
                    let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1000 })
                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getUnRead, token: 'getUnRead' })
                        this.globals.send({ path: this.token.getlist, token: 'getlist' })
                    }
                    break
                default:
                    break
            }
        })
    }
    ngOnInit() {
        this.globals.send({ path: this.token.getlist, token: 'getlist' })
        this.cwstable._ini({
            cols: this.cols,
            data: [],
            keyword: 'cart',
            sorting: { field: 'day_start', sort: 'DESC', type: 'date' },
            count: 50
        })
        this.cartItem._ini({ cols: this.colsselect, data: [], keyword: 'cartItem' })
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    onShow(item, index) {
        this.id = item.id
        if (this.step == index) {
            this.step = -1
        } else {
            this.step = index
            this.getrow(item.id)
        }
    }

    getrow(id) {
        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: id } })
    }

    onChangeStatus(item, status) {
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'cart.changeStatus', name: item.code } })
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.step = -1
                this.globals.send({
                    path: this.token.changeStatus,
                    token: 'changeStatus',
                    params: { id: item.id, delivery_status: status }
                })
            }
        })
    }
}
