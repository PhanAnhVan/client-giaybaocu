import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Globals } from '../../../globals';
import { TranslateService } from '@ngx-translate/core';
@Component({
    selector: 'admin-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    @Output('eventOpened') eventOpened = new EventEmitter<number>();
    public company: any; connect;
    public step = 0;
    public expanded: boolean = true;
    public user: any = {}
    public token: any = {
        company: "api/company",
    }
    constructor(
        public globals: Globals,
        public translate: TranslateService,
    ) {
        this.user = this.globals.USERS.get(true);
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'company':
                    this.company = res.data;
                    break;

                default:
                    break;

            }
        });
    }
    ngOnInit() {
        this.globals.send({ path: this.token.company, token: 'company' });
    }
    onClickMenu = () => {
        this.eventOpened.emit()
    }
}