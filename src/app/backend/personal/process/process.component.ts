import { Component, OnInit } from '@angular/core'
import { Validators, FormBuilder, FormGroup } from '@angular/forms'
import { uploadFileService } from '../../../services/integrated/upload.service'
import { Router, ActivatedRoute } from '@angular/router'
import { Globals } from '../../../globals'
import { ToastrService } from 'ngx-toastr'

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit {
    public fm: FormGroup

    public id: number = 0

    private connect

    public avatar = new uploadFileService()

    public token: any = {
        process: 'set/user/process',

        getrow: 'get/user/getrow'
    }
    constructor(
        private fb: FormBuilder,
        private router: Router,

        public globals: Globals,
        private toastr: ToastrService,

        private routerAct: ActivatedRoute
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id']
        })
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case 'getrow':
                    let data = res['data']

                    this.fmConfigs(data)

                    break
                case 'process':
                    let type = res['status'] == 1 ? 'success' : res['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1000 })

                    if (res['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate(['admin/user/get-list'])
                        }, 2000)
                    }
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        if (this.id && this.id != 0) {
            this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } })
        } else {
            this.fmConfigs()
        }
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    fmConfigs(data: any = '') {
        data = typeof data === 'object' ? data : { status: 1, sex: 1, birth_date: new Date() }

        const imagesConfig = { path: this.globals.BASE_API_URL + 'public/avatar/', data: data.avatar ? data.avatar : '' }

        this.avatar._ini(imagesConfig)

        this.fm = this.fb.group({
            name: [data.name ? data.name : '', [Validators.required]],

            code: [data.code ? data.code : ''],

            birth_date: data.birth_date ? new Date(data.birth_date) : null,

            sex: data.sex || null,

            phone: [data.phone ? data.phone : '', [Validators.required, Validators.pattern('^[0-9]*$')]],

            email: [
                data.email ? data.email : '',
                [
                    Validators.required,
                    Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)
                ]
            ],

            address: data.address ? data.address : '',

            password: [data.password ? data.password : '', [Validators.required, Validators.minLength(8)]],

            note: data.note ? data.note : '',

            status: data.status && data.status == 1 ? true : false
        })

        if (data.code) {
            this.fm.controls['code'].disable()
        }
    }
    onSubmit() {
        if (this.fm.valid) {
            let data = this.fm.value

            data.avatar = this.avatar._get(true)

            data.status = data.status == true ? 1 : 0

            this.globals.send({ path: this.token.process, token: 'process', data: data, params: { id: this.id } })
        }
    }
}
