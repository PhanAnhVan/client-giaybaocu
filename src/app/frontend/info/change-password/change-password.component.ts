import { Component, OnInit, OnDestroy } from '@angular/core'
import { Globals } from '../../../globals'
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { FormGroup, FormBuilder } from '@angular/forms'
import { ToastrService } from 'ngx-toastr'

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
    public connect

    fmchangePassword: FormGroup

    public id: any
    public type = 'password'
    public type2 = 'password'
    public width = document.body.getBoundingClientRect().width

    public token: any = {
        changePassword: 'api/changePasswordCustomer'
    }

    constructor(
        public fb: FormBuilder,
        public globals: Globals,
        private routerAct: ActivatedRoute,
        public translate: TranslateService,
        public router: Router,
        private toastr: ToastrService
    ) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'changePassword':
                    let type = response['status'] == 1 ? 'success' : response['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](response['message'], type, { timeOut: 1000 })
                    if (response['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate(['/'])
                        }, 1000)
                    }
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        let data = this.globals.CUSTOMER.get(true)
        this.id = data.id
        this.fmConfigChange()
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    changePass = {
        show: false,
        confirm: () => {
            let data = this.fmchangePassword.value
            this.globals.send({ path: this.token.changePassword, token: 'changePassword', data: data })
        }
    }

    fmConfigChange() {
        this.fmchangePassword = this.fb.group({
            id: this.id,
            password: '',
            password_new: ''
        })
    }
}
