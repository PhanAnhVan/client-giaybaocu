import { Component, OnInit } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { Subscription } from 'rxjs'
import { Router } from '../../../../../node_modules/@angular/router'
import { Globals } from '../../../globals'

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    public connect: Subscription
    public token: any = {
        menu: 'api/getmenu'
    }
    public type = 'password'
    public category: any = { data: [] }
    public policy: any = { data: [] }
    public support: any = { data: [] }
    public menu: any = { data: [] }
    public payment: any = { data: [], list: {} }
    public width: number = 0

    constructor(public globals: Globals, public translate: TranslateService, public router: Router) {
        setTimeout(() => {
            if (window['hisTats']) {
                window['hisTats']()
            }
        }, 50)
        this.width = document.body.getBoundingClientRect().width
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'menuFooterCategory':
                    this.category.data = res.data
                    break

                case 'menuFooterSupport':
                    this.support.data = res.data
                    break

                case 'menuFooterPolicy':
                    this.policy.data = res.data
                    break

                case 'menuFooterMenu':
                    this.menu.data = res.data
                    break

                case 'menuFooterPayment':
                    for (let i = 0; i < res.data.length; i++) {
                        if (res.data[i].link == 'bo-cong-thuong') {
                            this.payment.list = res.data[i]
                        } else {
                            this.payment.data.push(res.data[i])
                        }
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.globals.send({ path: this.token.menu, token: 'menuFooterPolicy', params: { position: 'policy' } })

        this.globals.send({ path: this.token.menu, token: 'menuFooterCategory', params: { position: 'category' } })

        this.globals.send({ path: this.token.menu, token: 'menuFooterSupport', params: { position: 'support' } })

        this.globals.send({ path: this.token.menu, token: 'menuFooterMenu', params: { position: 'end' } })

        // this.globals.send({ path: this.token.menu, token: 'menuFooterPayment', params: { position: 'payment' } })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getType = (link: string, type: string | number) => {
        switch (+type) {
            case 1:
            case 2:
                link = link
                break
            case 3:
                link = 'san-pham/' + link
                break
            case 4:
                link = 'tin-tuc/' + link
                break
            default:
                break
        }

        return link
    }
}
