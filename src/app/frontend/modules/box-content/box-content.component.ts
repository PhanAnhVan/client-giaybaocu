import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-box-content',
    templateUrl: './box-content.component.html',
    styleUrls: ['./box-content.component.css'],


})
export class BoxContentComponent {

    @Input('item') item: any;
    public width = document.body.getBoundingClientRect().width;
    constructor() { }
}