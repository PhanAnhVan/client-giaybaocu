import { Component, HostListener, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Globals } from 'src/app/globals'
import { CartService } from 'src/app/services/apicart/cart.service'
import { ToslugService } from 'src/app/services/integrated/toslug.service'

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    providers: [ToslugService]
})
export class HeaderComponent implements OnInit, OnDestroy {
    public company: any = {}

    width: number = innerWidth

    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,
        public toSlug: ToslugService,
        public routerAtc: ActivatedRoute,
        public cartApi: CartService
    ) {
        if (window['fixmenu']) {
            window['fixmenu']()
        }
        this.width = document.body.getBoundingClientRect().width
    }

    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.menuMobile.onMenu(true)
            }
        })
    }
    ngOnDestroy() {}

    public menuMobile = {
        show: true,
        showMobile: true,
        onMenu: (show: boolean) => {
            // TODO: fixed Mobile menu when scroll
            const HEADER = document.getElementById('header')
            !show ? HEADER.classList.add('position-fixed') : HEADER.classList.remove('position-fixed')

            if (this.menuMobile.showMobile == false) {
                this.menuMobile.onShowSearchMobile(true)
            }
            this.menuMobile.show = show
            let elm2 = document.getElementById('menu-mobi')

            if (this.menuMobile.show == false) {
                elm2?.classList.add('menu-mobi-block')
                elm2?.classList.remove('menu-mobi-hidden')
            } else {
                elm2?.classList.add('menu-mobi-hidden')
                elm2?.classList.remove('menu-mobi-block')
            }
        },
        onShowSearchMobile: (show: boolean) => {
            if (this.menuMobile.show == false) {
                this.menuMobile.onMenu(true)
            }
            this.menuMobile.showMobile = show
            let elm = document.getElementById('header')

            let elm2 = document.getElementById('search-mobi')
            if (this.menuMobile.showMobile == false) {
                elm2.classList.add('menu-mobi-block')
                elm2.classList.remove('menu-mobi-hidden')
                elm.classList.add('navbar-fixed')
            } else {
                elm2.classList.add('menu-mobi-hidden')
                elm2.classList.remove('menu-mobi-block')
                elm.classList.remove('navbar-fixed')
            }
        }
    }

    public search = {
        token: '',

        icon: <boolean>false,

        value: '',

        onSearch: () => {
            this.search.value = this.toSlug._ini(this.search.value.replace(/\s+/g, ' ').trim())

            if (this.search.check_search(this.search.value)) {
                this.router.navigate(['/tim-kiem/' + this.search.value])

                document.getElementById('search').blur()
            }

            this.search.value = ''

            this.search.icon = false
        },

        check_search: value => {
            let skip = true
            skip =
                value.toString().length > 0 &&
                value != '' &&
                value != '-' &&
                value != '[' &&
                value != ']' &&
                value != '\\' &&
                value != '{' &&
                value != '}'
                    ? true
                    : false
            return skip
        },

        showSearch: () => {
            this.search.icon = !this.search.icon
        }
    }

    @HostListener('window:scroll', ['$event'])
    onWindowScroll() {
        let elm2 = document.getElementById('search-mobi')
        elm2?.classList.add('menu-mobi-hidden')
        elm2?.classList.remove('menu-mobi-block')
        this.menuMobile.showMobile = true
    }

    cart = {
        close: () => {
            document.getElementById('notification').classList.remove('d-block')
        },
        router: () => {
            this.router.navigate(['/gio-hang'])
            document.getElementById('notification').classList.remove('d-block')
        }
    }

    logout() {
        this.globals.CUSTOMER.remove()
        setTimeout(() => {
            this.router.navigate(['/trang-chu'])
        }, 200)
    }
}
