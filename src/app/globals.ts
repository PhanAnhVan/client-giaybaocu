import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'

export class configOptions {
    path: string
    data?: Object
    params?: Object
    token: string
}

@Injectable()
export class Globals {
    public BASE_API_URL = 'https://gio22.giowebsite.com/'

    public admin: string = 'admin'
    public debug: Boolean = false
    private res = new Subject<string>()
    public company: any = {}
    public result = this.res.asObservable()
    public configCkeditor = {
        filebrowserBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Flash',
        filebrowserUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    }

    constructor(private http: HttpClient, public router: Router) {
        this.result.subscribe((res: any) => {
            if (this.debug) {
                console.log(res)
            }
        })
    }
    send = (option: configOptions) => {
        if (option.path && option.token) {
            // kiểm tra token
            let params = () => {
                let param = '?mask=' + option.token
                if (option.params) {
                    let keys = Object.keys(option.params)
                    for (let i = 0; i < keys.length; i++) {
                        if (i == 0) {
                            param += '&'
                        }
                        param += keys[i] + '=' + option.params[keys[i]]
                        param += i + 1 == keys.length ? '' : '&'
                    }
                }
                return param
            }
            this.http.post(this.BASE_API_URL + option.path + params(), option.data).subscribe((result: any) => {
                this.res.next(result)
            })
        }
    }
    public time = {
        format: () => {},
        date: e => {
            e = typeof e === 'object' ? e : new Date()
            return e.getFullYear() + '-' + (e.getMonth() + 1).toString() + '-' + e.getDate()
        }
    }

    public price = {
        format: (price: { toString: () => string }) => {
            return typeof price == 'number' ? price : +price.toString().replace(/\./g, '')
        },
        change: (price: any) => {
            let value = price.toString().replace(/\./g, '')
            let val = isNaN(value) ? 0 : +value
            let res = isNaN(value) ? 0 : val < 0 ? 0 : value
            return (price = Number(res).toLocaleString('vi'))
        }
    }

    public USERS = {
        token: 'users',
        store: 'localStorage',
        _check: () => {
            return this.http.post(this.BASE_API_URL + 'api/login/check', this.USERS.get(true))
        },
        get: skip => {
            if (skip == true) {
                return window.localStorage.getItem(this.USERS.store) ? JSON.parse(window.localStorage.getItem(this.USERS.store)) : {}
            } else {
                return window.localStorage.getItem(this.USERS.token) ? window.localStorage.getItem(this.USERS.token) : null
            }
        },
        check: skip => {
            return skip == true
                ? window.localStorage.getItem(this.USERS.store)
                    ? true
                    : false
                : window.localStorage.getItem(this.USERS.token)
                ? true
                : false
        },
        set: (data, skip) => {
            data = typeof data === 'object' ? JSON.stringify(data) : data
            if (skip == true) {
                window.localStorage.setItem(this.USERS.store, data)
            } else {
                window.localStorage.setItem(this.USERS.token, data)
            }
        },
        remove: (skip: any = '') => {
            if (!skip) {
                window.localStorage.clear()
            } else {
                if (skip == true) {
                    window.localStorage.removeItem(this.USERS.store)
                } else {
                    window.localStorage.removeItem(this.USERS.token)
                }
            }
            this.router.navigate(['/login/'])
            return this.http.post(this.BASE_API_URL + 'api/logout/admin', {}).subscribe((result: any) => {
                this.res.next(result)
            })
        }
    }

    public CUSTOMER = {
        token: 'CUSTOMER',
        store: 'localStorageCUSTOMER',
        server: 'browsersIndexCheckloginCUSTOMER',
        mask: 'usertmpCUSTOMER',
        send: () => {
            //this.send({ path: this.USERS.server, data: { token: this.USERS.get(false) } });
        },
        get: skip => {
            if (skip == true) {
                return window.localStorage.getItem(this.CUSTOMER.store) ? JSON.parse(window.localStorage.getItem(this.CUSTOMER.store)) : {}
            } else {
                return window.localStorage.getItem(this.CUSTOMER.token) ? window.localStorage.getItem(this.CUSTOMER.token) : null
            }
        },
        check: skip => {
            return skip == true
                ? window.localStorage.getItem(this.CUSTOMER.store)
                    ? true
                    : false
                : window.localStorage.getItem(this.CUSTOMER.token)
                ? true
                : false
        },
        set: (data, skip) => {
            data = typeof data === 'object' ? JSON.stringify(data) : data
            if (skip == true) {
                window.localStorage.setItem(this.CUSTOMER.store, data)
            } else {
                window.localStorage.setItem(this.CUSTOMER.token, data)
            }
        },
        tmp: (data: any = '') => {
            if (typeof data === 'object') {
                window.localStorage.setItem(this.CUSTOMER.mask, JSON.stringify(data))
                return true
            } else {
                if (data == true) {
                    window.localStorage.removeItem(this.CUSTOMER.mask)
                    return true
                } else {
                    if (window.localStorage.getItem(this.CUSTOMER.mask)) {
                        try {
                            return JSON.parse(window.localStorage.getItem(this.CUSTOMER.mask))
                        } catch (error) {
                            return false
                        }
                    } else {
                        return false
                    }
                }
            }
        },
        remove: (skip: any = '') => {
            if (!skip) {
                window.localStorage.clear()
            } else {
                if (skip == true) {
                    window.localStorage.removeItem(this.CUSTOMER.store)
                } else {
                    window.localStorage.removeItem(this.CUSTOMER.token)
                }
            }
        },
        navigate: () => {
            this.router.navigate(['/login'])
        },
        _check: () => {
            return this.http.post(this.BASE_API_URL + 'api/checklogged', {})
        }
    }
}
