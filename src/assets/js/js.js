function viewLibrary() {
    $('.fancybox').fancybox({
        thumbs: { autoStart: true }
    })
    $('[data-fancybox]').fancybox({
        thumbs: { autoStart: true }
    })
}

$(document).ready(function () {
    $('body').tooltip({ selector: '[data-toggle=tooltip]' })
})

function fixmenu() {
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 200) {
            $('.menu').addClass('navbar-fixed')
        } else {
            $('.menu').removeClass('navbar-fixed')
        }
    })
}

function scrolltop() {
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 200) {
            $('.scroll-top').fadeIn(200)
        } else {
            $('.scroll-top').fadeOut(200)
        }
    })
    $('.scroll-top').on('click', function () {
        $('html,body').animate({ scrollTop: 0 }, 1500)
        return false
    })
}

function libraryGallary() {
    var $container = $('.animate-grid .gallary-thumbs')
    $container.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    })
    $('.animate-grid .categories a').click(function () {
        $('.animate-grid .categories .active').removeClass('active')
        $(this).addClass('active')
        var selector = $(this).attr('data-filter')
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        })
        return false
    })
}

/*  
var _Hasync = _Hasync || [];
function hisTats() {
    _Hasync.push(['Histats.start', '1,4527274,4,336,112,62,00011111']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function () {
        var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();

    setTimeout(() => {
        let length = document.querySelectorAll('img').length;
        document.querySelectorAll('img')[length - 1].style.display = "none";
        document.querySelectorAll('img')[length - 2].style.display = "none";
    }, 4000);
}
*/

/* 
    const data = [
  {code: 'Hugo40ml', name: 'Nước hoa Hugo 40 ml', price: '760000'},
{code: 'poloclub75ml', name: 'Nước hoa Poloclub 75ml', price: '690000'},
{code: 'nuochoalacoste75', name: 'Nước hoa Lacoste 75 ml', price: '1190000'},
{code: 'lipstick10oz', name: 'Lipstick 10 oz', price: '390000'},
{code: 'Tommy30ml', name: 'Nước hoa Tommy 30 ml', price: '490000'},
{code: 'salonpas140', name: 'Salonpas 140', price: '606000'},
{code: 'Lacoste125ml', name: 'Nước hoa Lacoste 125 ml', price: '1590000'},
{code: 'prenatal150', name: 'Nature Made Prenatal Multi + DHA - Vitamin Tổng Hợp Cho Bà Bầu Của Mỹ', price: '820000'},
{code: 'Sensodyne 5', name: 'Sensodyne 5', price: '900000'},
{code: 'poloblack125ml', name: 'Polo Black 125 ml', price: '2090000'},
{code: 'namHugo75ml', name: 'Nước hoa nam Hugo 75 ml', price: '1190000'},
{code: 'fishoil400', name: 'Viên Uống Fish Oil Dầu Cá Kirkland 400 Viên', price: '450000'},
{code: 'Tommy100ml', name: 'Nước hoa Tommy Nam  100ml', price: '950000'},
{code: 'BENGAY', name: 'KEM XOA BÓP BENGAY ULTRA STRENGTH GIẢM ĐAU NHỨC XƯƠNG KHỚP CỦA MỸ', price: '0'},
{code: 'CLEAR EYES', name: 'Thuốc nhỏ mắt Clear Eyes Redness Relief', price: '125000'},
{code: 'ONE1DAY Nữ', name: 'Viên Bổ Sung Vitamin Tổng Hợp Cho Nữ Dưới 50', price: '620000'},
{code: 'GIÀY DANSKIN', name: 'GIÀY DANSKIN', price: '0'},
{code: 'MOVE FREE', name: 'Hỗ trợ xương khớp Viên uống bổ khớp Schiff Move Free Joint Health Advanced', price: '680000'},
{code: 'COLLAGEN', name: 'Nước Dạng Ống Collagen Liquid Drink Mix 4000Mg Của Mỹ', price: '680000'},
{code: 'ONE1DAY MEN', name: 'Vitamin Tổng Hợp One A Day MEN’s Health Formula', price: '620000'},
{code: 'TOMMY GIRL', name: 'Nước hoa Tommy Nu 100ml', price: '950000'},
{code: 'VITAMINC', name: 'Vitamin C 1000mg Kirkland Hộp 500 Viên', price: '590000'},
{code: 'GLUCOSAMINE', name: 'Thuốc bổ khớp Kirkland Glucosamine', price: '590000'},
{code: 'OMEGA369', name: 'Omega 3 6 9 Member’s Mark Supports Heart Health Của Mỹ Hộp 325 Viên', price: '640000'},
{code: 'DAUNUOCXANH', name: 'DAU NUOC XANH', price: '180000'},
{code: 'OPBLACK', name: 'OP BLACK FOR HIM', price: '1120000'},
{code: 'COOL WATER', name: 'Nước hoa Davidoff Cool Water Woman', price: '1320000'},
{code: 'MODERN', name: 'Banana Republic Modern', price: '1320000'},
{code: 'PARIS HILTON', name: 'PARIS HILTON', price: '860000'},
{code: 'BREAK FREE', name: 'BREAK FREE- AMERICAN', price: '1240000'},
{code: 'FEAR NOTHING', name: 'FEAR NOTHING- AMERICAN', price: '1240000'},
{code: 'COQ10', name: 'COQ10 TIM MACH', price: '520000'},
{code: 'SUPER B COMPLEX', name: 'SUPER B COMPLEX', price: '470000'},
{code: 'GREEN TEA', name: 'GREEN TEA TRA GIAM CAN', price: '590000'},
{code: 'EXTRA STRENGTH D3', name: 'EXTRA STRENGTH D3', price: '400000'},
{code: 'FOCUS FACTOR', name: 'FOCUS FACTOR BO NAO', price: '610000'},
{code: 'SKINSUCCESS', name: 'SKINSUCCESS KEM CHAM SOC DA', price: '350000'},
{code: 'SERUM VITAMIN C', name: 'PERRICONE + SERUM VITAMIN C', price: '870000'},
{code: 'COLD_FLU', name: 'COLD_FLU', price: '450000'},
{code: 'IRON', name: 'IRON CHAT SAC', price: '450000'},
{code: 'CRANBERRY', name: 'Trunature Viên Uống Hỗ Trợ Đường Tiết Niệu Cranberry 140 Viê', price: '590000'},
{code: 'OSTEO BIFLEX', name: 'Viên Bổ Khớp Osteo Bi-Flex 200 Viên Mỹ Giá Chính Hãng', price: '680000'},
{code: 'HAIR-SKIN-NAIL', name: 'Hair Skin Nail Viên Uống Đẹp Da , Tóc Và Móng Của Mỹ', price: '470000'},
{code: 'Evening Primrose Oil', name: 'Trunature Tinh Dầu Hoa Anh Thảo Evening Primrose Oil', price: '550000'},
{code: 'CENTRUM', name: 'Vitamin tổng hợp Centrum Silver Adults 50+', price: '660000'},
{code: 'ALLEGRA', name: 'Viên chống dị ứng cho người lớn Allegra Adult 24 Hour Allergy 180mg 70 viên', price: '1090000'},
{code: 'VITAMIN E', name: 'Viên Uống Vitamin E Kirkland Signature Vitamin E 400', price: '450000'},
{code: 'CALCIUM', name: 'Viên Uống Bổ Sung Canxi Kirkland Signature Calcium 600mg + D3', price: '450000'},
{code: 'BIOTIN', name: 'ViênUống Mọc Tóc Biotin Của Mỹ', price: '390000'},
{code: 'GARLIC 1250', name: 'Nature Made Viên Uống Tinh Dầu Tỏi Giúp Ổn Định Cholesteron 1250mg Garlic 100 Viên', price: ,'560000'}
{code: 'Armani Code', name: 'Nước hoa Armani Code Giorgio Armani', price: '2690000'},
{code: 'LA VIE EST BELLE', name: 'Nước Hoa La Vie Est Belle Pháp', price: '1580000'},
{code: 'SLEEP3', name: 'SLEEP3', price: '590000'},
{code: 'TOMMY WALLET', name: 'BOP NAM Tommy hilfiger wallet', price: '690000'},
{code: 'BAN CHAY PIN', name: 'Bàn chải Pin US', price: '160000'},
{code: 'Acetaminophen', name: 'Thực phẩm chức năng Thuốc giảm đau Kirkland Extra Strength Acetaminophen 500mg', price: ,'390000'}
{code: 'PROBIOTIC', name: 'Viên uống tốt cho hệ tiêu hóa trunature Advanced Digestive Probiotic 100 viên', price: '460.,000'}
{code: 'COLLAGEN BOT', name: 'SUPER COLLAGEN DANG BOT', price: '690000'},
{code: 'SON MAC', name: 'LIPSTICK SON MAC', price: '390000'},
{code: 'CHỈ NHA KHOA', name: 'CHỈ NHA KHOA', price: '50000'},
{code: 'VICTORIA SECRET', name: 'VICTORIA SECRET', price: '1290000'},
{code: 'VERSACE', name: 'VERSACE MINI 5ML', price: '390000'},
{code: 'MICHEAL KOR', name: 'MICHEAL KOR SET', price: '1290000'},
{code: 'WHITE DIAMOND', name: 'WHITE DIAMOND SET', price: '1200000'},
{code: 'SECRET', name: 'Lăn khử mùi secret Mỹ', price: '600000'},
{code: 'ORAL - B', name: 'BAN CHAI ORAL - B', price: '380000'},
{code: 'Gift Set Victoria’s Secret', name: 'Gift Set Victoria’s Secret', price: '1090000'},
{code: 'COLLAGEN VIEN', name: 'Viên uống collagen youtheory 390 viên', price: '620000'},
{code: 'COMBO TRESOR', name: 'Giftset Nước Hoa Lancôme Tresor', price: '2190000'},
{code: 'GINKGO BO NAO', name: 'Viên Uống Bổ Não Trunature Ginkgo', price: '590000'},
{code: 'KEO BAO TU ANTACID', name: 'KEO BAO TU ANTACID', price: '360000'},
{code: 'MultiVites Gummy', name: 'Kẹo dẻo bổ sung Vitamin Vitafusion MultiVites Gummy (260 viên)', price: '470000'},
{code: 'ONE A DAY GUMMY', name: 'Kẹo dẻo đa Vitamin dành cho nữ One a Day Women Vitacraves Gummies 230 viên', price: '560.,000'}
{code: 'ONE A DAY 50+', name: 'ONE A DAY 50+ adult', price: '620000'},
{code: 'Polo Club Classic 50ml', name: 'Nước hoa nam Polo Club Classic Eau de Toilette 50ml của Mỹ - Polo Classic 50m', ,price: '590000'}
{code: 'Kem chống nắng Neutrogena', name: 'Kem chống nắng Neutrogena Ultra Sheer SPF 100 PA +++', price: '420000'},
{code: 'BEAUTY SLEEP', name: 'BEAUTY SLEEP KEO NGU', price: '690000'},
{code: 'KILIAN', name: 'KILIAN NUOC HOA NAM', price: '7700000'},
{code: 'LIVE LUXE', name: 'LIVE LUXE NUOC HOA NU', price: '1390000'},
{code: 'OLAY 24', name: 'Olay Regenerist Collagen Peptide 24 Day Cream Without Fragrance, 50ml', price: '1150000'},
{code: 'ADVIL', name: 'Thuốc giảm đau hạ sốt Advil 360 viên của Mỹ', price: '620000'},
{code: 'Viên uống tinh chất nghệ', name: 'Viên uống tinh chất nghệ Youtheory Turmeric 1000mg 180 viên', price: '1150.,000'}
{code: 'Son dưỡng môi Carmex', name: 'Son dưỡng môi Carmex', price: '150000'},
{code: 'Ocuvite', name: 'Viên bổ mắt Ocuvite dành cho người lớn tuổi Ocuvite Adult 50+ 90 Softgels', price: '690000'},
{code: 'Fiber Well', name: 'Kẹo dẻo bổ sung chất xơ Vitafusion Fiber Well 220 viên', price: '680000'},
{code: 'TYLENOL290', name: 'Viên Uống Giảm Đau Hạ Sốt Tylenol Rapid Release Gels 290 Viên', price: '550000'},
{code: 'TYLENOL325', name: 'Viên uống giảm đau hạ sốt Tylenol 325', price: '520000'},
{code: 'TYLENOL100', name: 'Giảm đau hạ sốt Tylenol Rapid Release Gels 500mg hộp 100', price: '380000'},
{code: 'TYLENOL50', name: 'GIẢM ĐAU HẠ SỐT TYLENOL RAPID RELEASE GELS 500MG HỘP 50 VIÊN', price: '260000'},
{code: 'TYLENOL24', name: 'GIẢM ĐAU HẠ SỐT TYLENOL RAPID RELEASE GELS 500MG HỘP 24 VIÊN', price: '130000'},
{code: 'TYLENOL6', name: 'GIẢM ĐAU HẠ SỐT TYLENOL RAPID RELEASE GELS 500MG HỘP 6 VIÊN', price: '90000'},
{code: 'SIRO TYLENOL', name: 'Tylenol chai 120ml siro giúp giảm các cơn đau, hạ sốt cho trẻ', price: '350000'},
{code: 'CHEWABLE VITAMIN C', name: 'CHEWABLE VITAMIN C', price: '4800000'},
{code: 'OSTELIN CALCIUM', name: 'OSTELIN CALCIUM', price: '780000'},
{code: 'TYLENOL COUGH', name: 'TYLENOL COUGH SIRO', price: '350000'},
{code: 'PRENATAL', name: 'PRENATAL BAU', price: '680000'},
{code: 'ADVIL T10', name: 'ADVIL COVID', price: '550000'}
]
*/
